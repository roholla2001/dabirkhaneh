package ray.service;

import java.util.List;

import ray.sample.model.*;
import ray.sample.view.*;
import ray.util.hibernate.DBManager;

public class LetterService {
	public static List<Letter> selectAllLetters() {
		List<Letter> list = (List<Letter>)DBManager.find("from Letter");
				
		return list;
	}
	
	public static Letter selectLetter(long id) {
		StringBuilder query = new StringBuilder("from Letter where ");
		
		query.append("id = ");
		query.append(id);
		
		
		List<Letter> list = (List<Letter>)DBManager.find(query.toString());
				
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	public static void addLetter(Letter letter) {
		try {
			DBManager.save(letter);
		} catch (Exception e) {
			System.err.println("%%%could not save letter%%%");
			e.printStackTrace();
		}
	}
	
	public static List<SignerView> getSignerViews(long receiverId) {
		StringBuilder query = new StringBuilder("from SignerView where ");
		
		query.append("receiverId = ");
		query.append(receiverId);
		
		query.append(" and ");
		
		query.append("hasSigned = 0");
		
		
		List<SignerView> list = (List<SignerView>)DBManager.find(query.toString());
				
		return list;
	}
	
	public static List<SentListView> getSentViewsFrom(long senderId) {
		StringBuilder query = new StringBuilder("from SentListView where ");
		
		query.append("senderId = ");
		query.append(senderId);
		
		
		List<SentListView> list = (List<SentListView>)DBManager.find(query.toString());
				
		return list;
	}
	
	public static SentListView getSentView(long letterId) {
		StringBuilder query = new StringBuilder("from SentListView where ");
		
		query.append("id = ");
		query.append(letterId);
		
		
		List<SentListView> list = (List<SentListView>)DBManager.find(query.toString());
				
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	
	public static User getSender(long letterId) {
		StringBuilder query = new StringBuilder("from Delivery d where ");
		
		query.append("letterId = ");
		query.append(letterId);
		
		query.append(" limit 1");
		
		List<Delivery> list = (List<Delivery>)DBManager.find(query.toString());
				
		if(list.isEmpty())
			return null;
		
		long senderId = list.get(0).getSenderId();
		
		User sender = UserService.selectUser(senderId);
		
		return sender;
	}

	public static Letter updateStatus(long id, int status) {
		
		return null;
	}
	
	public static Status getStatusEnum(int status) {
		return Status.values()[status];
	}
	
	public static String getStatusString(int status) {
		return getStatusEnum(status).toString();
	}
	
	enum Status {
		SENT, IN_PROCESS, DENIED, 
		ACCEPTED, ARCHIVED;

		@Override
		public String toString() {
			switch(this) {
				case SENT:
					return "فرستاده شده";
				case IN_PROCESS:
					return "در حال بررسی";
				case DENIED:
					return "رد شده";
				case ACCEPTED:
					return "تایید شده";
				case ARCHIVED:
					return "بایگاانی شده";
				default:
					return "STATUS";
			}
			
		}
	}

	public static void archive(List<SentListView> selectedLetters) {
		StringBuilder ids = new StringBuilder("(");
		for(int i = 0; i < selectedLetters.size(); i++) {
			if(i > 0)
				 ids.append( ", ");
			ids.append(selectedLetters.get(i).getId());
		}
		ids.append(")");
		
		List<Letter> list = (List<Letter>)DBManager.find("from Letter where id in " + ids);
		
		try {	
			for(Letter letter: list) {
				letter.setStatus(4); // 4: archived
				DBManager.update(letter);
			}
		}
		catch (Exception e) {
			System.err.println("couldn't archive selected letters!!!");
			e.printStackTrace();
		}
		
		
	}
}
