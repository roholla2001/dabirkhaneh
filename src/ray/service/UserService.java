package ray.service;

import java.util.List;
import ray.sample.model.*;

import ray.util.hibernate.DBManager;

public class UserService {
	public static User selectUser(String username, String password) {
		StringBuilder query = new StringBuilder("from User where ");
		
		query.append("userName = ");
		query.append("\'" + username + "\'");
		
		query.append(" and ");
		
		query.append("pass = ");
		query.append("\'" + password + "\'");
		
		List<User> list = (List<User>)DBManager.find(query.toString());
				
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	
	public static List<User> selectAllUsers() {
		List<User> list = (List<User>)DBManager.find("from User");
				
		return list;
	}
	
	public static User selectUser(long id) {
		StringBuilder query = new StringBuilder("from User where ");
		
		query.append("id = ");
		query.append(id);
		
		
		List<User> list = (List<User>)DBManager.find(query.toString());
				
		if(list.size() > 0)
			return list.get(0);
		else
			return null;
	}


	public static List<User> selectAllUsersExcept(long id) {
		StringBuilder query = new StringBuilder("from User where ");
		
		query.append("id != ");
		query.append(id);
		
		List<User> list = (List<User>)DBManager.find(query.toString());
				
		return list;
	}
}
