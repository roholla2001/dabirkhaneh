package ray.service;

import java.util.*;

import ray.sample.model.*;
import ray.sample.view.SentListView;
import ray.util.hibernate.DBManager;

public class DeliveryService {
	
	public static void addDelivery(Delivery delivery) {
		try {
			DBManager.save(delivery);
		} catch (Exception e) {
			System.err.println("%%%could not save delivery%%%");
			e.printStackTrace();
		}
	}

	public static void addDeliveries(Set<Delivery> deliveries) {
		try {
			DBManager.saveSet(deliveries);
		} catch (Exception e) {
			System.err.println("%%%could not save deliveries%%%");
			e.printStackTrace();
		}
	}

	public static List<Delivery> getDeliveiresFrom(long userId) {
		StringBuilder query = new StringBuilder("from Delivery d where ");
		
		query.append("senderId = ");
		query.append(userId);
		
		List<Delivery> list = (List<Delivery>)DBManager.find(query.toString());
				
		return list;
	}

	public static List<Delivery> getDeliveiresTo(long userId) {
		StringBuilder query = new StringBuilder("from Delivery where ");
		
		query.append("receiverId = ");
		query.append(userId);
		
		List<Delivery> list = (List<Delivery>)DBManager.find(query.toString());
				
		return list;
	}


	public static void sign(long signerId, long letterId) {
		StringBuilder query = new StringBuilder("from Delivery where ");
		
		query.append("letterId = ");
		query.append(letterId);
		
		query.append(" and ");
		
		query.append("receiverId = ");
		query.append(signerId);
		
		query.append(" and ");
		
		query.append("isSigner = 1");
		
		List<Delivery> list = (List<Delivery>)DBManager.find(query.toString());
		
		if(list.isEmpty())
			return;
		
		Delivery del = list.get(0);
		del.setSigned(1); // 1 means sign
		
		try {
			DBManager.update(del);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

	public static void reject(long signerId, long letterId) {
		StringBuilder query = new StringBuilder("from Delivery where ");
		
		query.append("letterId = ");
		query.append(letterId);
		
		query.append(" and ");
		
		query.append("receiverId = ");
		query.append(signerId);
		
		query.append(" and ");
		
		query.append("isSigner = 1");
		
		List<Delivery> list = (List<Delivery>)DBManager.find(query.toString());
		
		if(list.isEmpty())
			return;
		
		Delivery del = list.get(0);
		del.setSigned(2); // 2 means reject
		
		try {
			DBManager.update(del);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static List<SentListView> getWaitForArchives() {
		StringBuilder query = new StringBuilder("from SentListView where ");
		
		query.append("status in (2, 3)");
		
		List<SentListView> list = (List<SentListView>)DBManager.find(query.toString());
				
		return list;
	}

	public static List<SentListView> getArchiveds() {
		StringBuilder query = new StringBuilder("from SentListView where ");
		
		query.append("status=4");
		
		List<SentListView> list = (List<SentListView>)DBManager.find(query.toString());
				
		return list;
	}
	
	
}
