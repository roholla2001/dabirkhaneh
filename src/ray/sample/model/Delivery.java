package ray.sample.model;

public class Delivery {
	
	private long id;
	private long letterId;
	private long senderId;
	private long receiverId;
	
	private boolean signer;
	private int signed;
	private boolean viewed;
	
	public Delivery() {
		
	}
	
	
	
	
	public Delivery(long letterId, long senderId, long receiverId, boolean signer) {
		this.letterId = letterId;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.signer = signer;
		signed = 0;
		viewed = false;
	}




	// getters & setters

	
	
	public long getLetterId() {
		return letterId;
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public void setLetterId(long letterId) {
		this.letterId = letterId;
	}

	public long getSenderId() {
		return senderId;
	}

	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	public long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}


	public boolean isSigner() {
		return signer;
	}


	public void setSigner(boolean signer) {
		this.signer = signer;
	}


	public int getSigned() {
		return signed;
	}


	public void setSigned(int signed) {
		this.signed = signed;
	}


	public boolean isViewed() {
		return viewed;
	}


	public void setViewed(boolean viewed) {
		this.viewed = viewed;
	}

	
	
	
}
