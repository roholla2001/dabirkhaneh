package ray.sample.model;



public class User {
	private long id;
	private String firstName;
	private String lastName;
	private String username;
	private String pass;
	
	public User() {
		
	}
	
	
	// getters & setters

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String userName) {
		this.username = userName;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	
	@Override
	public String toString() {
		return "User(" + id + ", " + getFullName() + ")";
	}
	
	
}
