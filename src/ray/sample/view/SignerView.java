package ray.sample.view;

import ray.sample.model.*;

public class SignerView {
	
	private long id; // also delivery id
	
	private long senderId;
	private long receiverId;
	private long letterId;
	
	private int hasSigned;
	
	private String senderFullName;
	private String letterTitle;
	
	
	
	public SignerView() {
		
	}

	
	// getters & setters
	
	
	public long getSenderId() {
		return senderId;
	}

	public int getHasSigned() {
		return hasSigned;
	}


	public void setHasSigned(int hasSigned) {
		this.hasSigned = hasSigned;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	public long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}

	public long getLetterId() {
		return letterId;
	}

	public void setLetterId(long letterId) {
		this.letterId = letterId;
	}

	public String getSenderFullName() {
		return senderFullName;
	}

	public void setSenderFullName(String senderFullName) {
		this.senderFullName = senderFullName;
	}

	public String getLetterTitle() {
		return letterTitle;
	}

	public void setLetterTitle(String letterTitle) {
		this.letterTitle = letterTitle;
	}
	
	
}
