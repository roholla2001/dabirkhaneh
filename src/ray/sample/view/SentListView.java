package ray.sample.view;

import ray.sample.model.*;
import ray.service.LetterService;

public class SentListView {
	
	private long id; // also the letter id
	
	private long senderId;
	private int signedCount;
	private int rejectedCount;
	private int signerCount;
	
	private String title;
	
	private String senderFullName;
	private String viewersFullNames;
	private String signersFullNames;
	
	private int status;
	
	public SentListView() {
	}

	
	// getters & setters
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	// return Farsi status
	public int getStatus() {
		return status;
	}
	
	public long getId() {
		return id;
	}

	public int getSignedCount() {
		return signedCount;
	}


	public void setSignedCount(int signedCount) {
		this.signedCount = signedCount;
	}


	public int getRejectedCount() {
		return rejectedCount;
	}


	public void setRejectedCount(int rejectedCount) {
		this.rejectedCount = rejectedCount;
	}


	public int getSignerCount() {
		return signerCount;
	}


	public void setSignerCount(int signerCount) {
		this.signerCount = signerCount;
	}


	public long getSenderId() {
		return senderId;
	}

	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}


	public String getSenderFullName() {
		return senderFullName;
	}

	public void setSenderFullName(String senderFullName) {
		this.senderFullName = senderFullName;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getViewersFullNames() {
		return viewersFullNames;
	}

	public void setViewersFullNames(String viewersFullNames) {
		this.viewersFullNames = viewersFullNames;
	}

	public String getSignersFullNames() {
		return signersFullNames;
	}

	public void setSignersFullNames(String signersFullNames) {
		this.signersFullNames = signersFullNames;
	}
	
	public String getStatusString() {
		return LetterService.getStatusString(status);
	}
	
}
