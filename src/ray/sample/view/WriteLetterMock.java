package ray.sample.view;

import ray.sample.model.*;
import java.util.*;



public class WriteLetterMock {
	private Letter letter;
	
	private List<User> otherUsers;
	
	private List<User> viewers;
	private int newViewerId;
	private String viewersFullNames;

	private List<User> signers;
	private int newSignerId;
	private String signersFullNames;
	
	public WriteLetterMock(List<User> otherUsers) {
		letter = new Letter();
		this.otherUsers = otherUsers;
		viewers = new ArrayList<User>();
		signers =  new ArrayList<User>();
		viewersFullNames = new String();
		signersFullNames = new String();
	}
	
	
	
	public void clearViewers() {
		viewers.clear();
		newViewerId = 0;
		viewersFullNames = "";
	}
	
	public void clearSigners() {
		signers.clear();
		newSignerId = 0;
		signersFullNames = "";
	}
	
	public void addNewViewer() {
		for(User user: otherUsers)
			if(user.getId() == newViewerId) {
				viewers.add(user);
				viewersFullNames += user.getFullName() + "\n";
				return;
			}
	}
	
	public void addNewSigner() {
		for(User user: otherUsers)
			if(user.getId() == newSignerId) {
				signers.add(user);
				signersFullNames += user.getFullName() + "\n";
				return;
			}
	}
	
	
	// getters & setters

	public Letter getLetter() {
		return letter;
	}

	public void setLetter(Letter letter) {
		this.letter = letter;
	}

	public List<User> getOtherUsers() {
		return otherUsers;
	}

	public void setOtherUsers(List<User> otherUsers) {
		this.otherUsers = otherUsers;
	}

	public void setViewers(List<User> viewers) {
		this.viewers = viewers;
	}
	

	public List<User> getViewers() {
		return viewers;
	}

	public List<User> getSigners() {
		return signers;
	}

	public void setSigners(List<User> signers) {
		this.signers = signers;
	}

	public int getNewViewerId() {
		return newViewerId;
	}

	public void setNewViewerId(int newViewerId) {
		this.newViewerId = newViewerId;
	}

	public int getNewSignerId() {
		return newSignerId;
	}

	public void setNewSignerId(int newSignerId) {
		this.newSignerId = newSignerId;
	}

	public String getViewersFullNames() {
		return viewersFullNames;
	}

	public void setViewersFullNames(String viewersFullNames) {
		this.viewersFullNames = viewersFullNames;
	}

	public String getSignersFullNames() {
		return signersFullNames;
	}

	public void setSignersFullNames(String signersFullNames) {
		this.signersFullNames = signersFullNames;
	}
	
	
}
