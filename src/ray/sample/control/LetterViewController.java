package ray.sample.control;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import ray.sample.model.*;
import ray.sample.view.*;
import ray.service.DeliveryService;
import ray.service.LetterService;
import ray.util.hibernate.DBManager;

@ViewScoped
@ManagedBean(name = "letter")
public class LetterViewController {
	
	User user;
	private SentListView view;
	private Letter letter;
	
	public LetterViewController() {
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		user = (User)sessionMap.get("user");
	}
	
	
	public void sign() {
		DeliveryService.sign(user.getId(), letter.getId());
		updateStatus();
	}
	
	public void reject() {
		DeliveryService.reject(user.getId(), letter.getId());		
		updateStatus();
	}
	
	private void updateStatus() {
		view = LetterService.getSentView(letter.getId());
		
		int status;
		
		if(view.getSignedCount() + view.getRejectedCount() == 0)
			status = 0;
		else if(view.getSignedCount() + view.getRejectedCount() == view.getSignerCount()) {
			if(view.getRejectedCount() > 0)
				status = 2;
			else
				status = 3;
		}
		else
			status = 1;
		
		letter.setStatus(status);
		
		try {
			DBManager.update(letter);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	// getters & setters
	
	public void setId(long id) {
		view = LetterService.getSentView(id);
		letter = LetterService.selectLetter(id);
	}
	
	public long getId() {
		return letter.getId();
	}
	
	public SentListView getView() {
		return view;
	}
	public void setView(SentListView letterView) {
		this.view = letterView;
	}
	public Letter getLetter() {
		return letter;
	}
	public void setLetter(Letter letter) {
		this.letter = letter;
	}
	
	
}
