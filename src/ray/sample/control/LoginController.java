package ray.sample.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.TabChangeEvent;

import ray.sample.model.User;
import ray.service.UserService;

@ViewScoped
@ManagedBean(name = "login")
public class LoginController {
	
	private User user;
		
	public LoginController() {
		user = new User();
	}
	
	
	public String doLogin() {
		
		User dbUser = UserService.selectUser(user.getUsername(), user.getPass());
		
		if(dbUser != null) {	
			user = dbUser;
			
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			sessionMap.put("user", user);
			
			return "home.xhtml?faces-redirect=true";
		}
		else {
			FacesContext context = FacesContext.getCurrentInstance();	
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"نام کاربری یا رمز عبور اشتباه است", null));
			
			return null;
		}
		
	}
	
	
	
	//getters & setters
	
	public String getUsername() {
		return user.getUsername();
	}

	public void setUsername(String username) {
		user.setUsername(username);
	}

	public String getPassword() {
		return user.getPass();
	}

	public void setPassword(String password) {
		user.setPass(password);
	}	
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
