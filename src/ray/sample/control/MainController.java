package ray.sample.control;

import ray.sample.model.*;
import ray.sample.view.*;
import ray.service.DeliveryService;
import ray.service.LetterService;
import ray.service.UserService;
import ray.util.hibernate.DBManager;

import java.util.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.NamedEvent;

import org.apache.commons.collections.map.HashedMap;
import org.apache.derby.tools.sysinfo;
import org.apache.derby.vti.IFastPath;
import org.primefaces.event.TabChangeEvent;

import freemarker.core.builtins.resolveBI;

@ViewScoped
@ManagedBean(name = "main")
public class MainController {
	
	private User user;
		
	private WriteLetterMock letterMock;
	
	private List<SignerView> signViews;
	private List<SentListView> sentViews;
	
	private List<SentListView> selectedLetters;
	
	public MainController() {
		FacesContext.getCurrentInstance().getExternalContext().getInitParameter("letterId"); // what is this for??
		// set the current user
		Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		user = (User)sessionMap.get("user");
		
		
		List <User> others = UserService.selectAllUsersExcept(user.getId());
		letterMock = new WriteLetterMock(others);
		
	}
	
	public void onTabChange(TabChangeEvent event) {
		System.out.println("\ntab changed to " + event.getTab().getId() + "\n");
		
		if(event.getTab().getId().equals("sendLetter")) {
			List <User> others = UserService.selectAllUsersExcept(user.getId());
			letterMock = new WriteLetterMock(others);	//TODO: add clear() method to letterMock and remove else
		}
		else {
			letterMock = null;
		}

		if(event.getTab().getId().equals("sign")) {
			signViews = LetterService.getSignerViews(user.getId());
		}
		
		if(event.getTab().getId().equals("sent")) {
			sentViews = LetterService.getSentViewsFrom(user.getId());
		}
		
	}
	
	
	public void sendLetter() {
		System.out.println("Sending letter...");
		Letter letter = letterMock.getLetter();
		LetterService.addLetter(letter);

		System.out.println("new letter added with id: " + letter.getId());		
		
		List<User> viewers = letterMock.getViewers();
		List<User> signers = letterMock.getSigners();
		
		Set<Delivery> deliveries = new HashSet<Delivery>(viewers.size() + signers.size());
		
		for(User viewer: viewers) {
			Delivery del = new Delivery(letter.getId(), user.getId(), viewer.getId(), false);  
			deliveries.add(del);
		}
		
		for(User viewer: signers) {
			Delivery del = new Delivery(letter.getId(), user.getId(), viewer.getId(), true);
			deliveries.add(del);
		}
		
		
		DeliveryService.addDeliveries(deliveries);
	}
	
	public void archiveSelected() {
		LetterService.archive(selectedLetters);
	}
	
	public void sayHi() {
		System.out.println("%%% HI %%%");
	}
	
	
	
	// getters & setters
	
	public List<SentListView> getWaitForArchives() {
		return DeliveryService.getWaitForArchives();
	}
	
	public List<SentListView> getArchiveds() {
		return DeliveryService.getArchiveds();
	}
		
	public User getUser() {
		return user;
	}

	public List<SentListView> getSelectedLetters() {
		return selectedLetters;
	}

	public void setSelectedLetters(List<SentListView> selectedLetters) {
		this.selectedLetters = selectedLetters;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public WriteLetterMock getLetterMock() {
		return letterMock;
	}
		
	public List<SignerView> getSignViews() {
		return signViews;
	}
	
	public List<SentListView> getSentViews() {
		return sentViews;
	}
	
	public String signLetter(Long letterId) {
		return "/signLetter.xhtml?faces-redirect=true&letterId="+letterId;
	}

}
